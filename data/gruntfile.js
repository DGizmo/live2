module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  // Project configuration.
  grunt.initConfig({

    autoprefixer: {
      options: {
        browsers: [
        'ie >= 10',
        'ff >= 20',
        'opera >= 12',
        'opera >= 21',
        'safari >= 6',
        'chrome >= 34',
        'android >= 4',
        'ios >= 6'
        ]
      },
      main: {
        src: ['js/serviceVoice/serviceVoice.css'],
        dest: 'js/serviceVoice/serviceVoice.css'
      }
    },
    // csswring: {

    //   options: {
    //     map               : true,
    //     removeAllComments : true
    //   },
    //   single_file: {
    //     src: 'js/serviceVoice/serviceVoice.css',
    //     dest: 'js/serviceVoice/serviceVoice.min.css'
    //   },
    //   multiple_files: {
    //     expand: true,
    //     flatten: true,
    //     src: 'css/*.css', // -> src/css/file1.css, src/css/file2.css
    //     dest: 'css/grunt/live2.min.css' // -> dest/css/file1.css, dest/css/file2.css
    //   }

    // }
    cssmin: {
      my_target: {
        files: [{
          expand: true,
          cwd: 'css/',
          src: ['*.css'],//, '!*.min.css'],
          dest: 'css/grunt/',
          ext: 'min.css'
        }]
      },
      combine: {
        files: {
          'js/serviceVoice/serviceVoice.min.css': ['js/serviceVoice/serviceVoice.min.css']
        }
      }
    },
    concat_css: {
      options: {},
      all: {
        src: ["css/grunt/*.css"],
        dest: "css/grunt/min/live2.css"
      },
    },
    uglify: {
      my_target: {
        options: {
          preserveComments : false
        },
        files: {
          'js/min/output.min.js': ['js/*.js'],
          'js/min/controller.min.js': ['js/controller/*.js']
        }
      }
    },
    watch: {
      scripts: {
        files: ['js/*.js' , 'js/controller/*.js'],
        tasks: ['uglify'],
        options: {
          spawn: false,
        }
      },
      css:{
        files: ['css/*.css'],
        tasks: ['autoprefixer','cssmin','concat_css'],
        options: {
          spawn: false,
        }

      } 
    }


  });

  grunt.registerTask('css',['autoprefixer', 'cssmin','concat_css']);

  // Default task(s).
  grunt.registerTask('default', ['css','uglify','watch']);


};