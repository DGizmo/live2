var MyValidate = angular.module('MyValidate.control', ['MyValidate.config'])
	.directive('myValidate', function(config,$rootScope) {
		return {
			restrict: 'A',
			require: 'form',
			link: function($scope, el, attrs, ctrl) {
				$scope.submited = false;
				var formName  = ctrl.$name;

				$scope.validate = function() {
			
					if(!ctrl.$invalid) {
						return true;
					}
					
					$scope.submited = true;
					return false;
				};

				$scope.hidemodel = function(){
					var popup = $('#basicModal');
					popup.modal('hide');
					$scope.$$prevSibling.$parent.company = {
						name : $scope.client.name
					};
				}


				$scope.$watch(formName + '.$invalid', function () {
					$scope.submited = false;
				});
			}
		};
	})
	.directive('myValidateClass', function(config) {
		return {
			restrict: 'A',
			require: '^form',
			link: function($scope, el, attrs, ctrl) {
				var formName  = ctrl.$name;
				var fieldName = attrs.myValidateClass;

				$scope.$watch(function() { 

					if ($scope.submited) {
						var field = $scope[formName][fieldName];
						var hasError = false;
						var errors = field.$error;

						for (var error in errors) {
							if (errors[error]) {
								hasError = true;
								break;
							}
						}

						if (hasError) { 
							el.addClass(config.class);
						} else {
							el.removeClass(config.class);
						}
					}
					return $scope.submited;
				});
			}
		};
	})
	.directive('myValidateMessage', function(config) {
		return {
			restrict: 'A',
			require: '^form',
			link: function($scope, el, attrs, ctrl) {
				var formName  = ctrl.$name;
				var fieldName = attrs.myValidateMessage;

				$scope.$watch(function() { 
					if ($scope.submited) {

						var field = $scope[formName][fieldName];
									
						if (field.$invalid) {
							var errors = field.$error;

							for (var error in errors) {
								if (errors[error]) {
									el.html(config.ERRORS[error]);
									break;
								}
							}

							el.css('display', '');
						} else {
							el.css('display', 'none');
						}

					}
					return $scope.submited; 
				});
			}
		};
	})
	.directive('validData', function(config) {
		return {
			require: 'ngModel',
			link: function($scope, el, attrs, ctrl) {
				if (!ctrl) return;

				var is_valid = function(s) {
					return ( ($scope.date<s) || s==undefined) ? true : false;
				};

				ctrl.$parsers.push(function(viewValue) {
					ctrl.$setValidity('valid-data', is_valid(viewValue));
					return viewValue;
				});
				ctrl.$formatters.push(function(modelValue) {
					ctrl.$setValidity('valid-data', is_valid(modelValue));
					return modelValue;
				});

			}
		};
	})
	.directive('existname', function(config) {
		return {
			require: 'ngModel',
			link: function(scope, el, attrs, ctrl) {
				if (!ctrl) return;

				var is_valid = function(s) {
					return ( typeof(s)=="object" || s==undefined || s=='') ? true : false;
				};

				ctrl.$parsers.push(function(viewValue) {
					el.value = 1;
					ctrl.$setValidity('existname', is_valid(viewValue));
					return viewValue;
				});
				ctrl.$formatters.push(function(modelValue) {
					ctrl.$setValidity('existname', is_valid(modelValue));
					return modelValue;
				});
			}
		};
	})
	.directive('focusAfterAjax',function($timeout){
	return {
		link: function(scope,el){
			var clearWatch = scope.$watch(function(){ return el;},function(){
				if(el){
					el[0].focus();
				}
			})
			}
		}
	})
	.directive('dtSorting', function() {
	  return {
	  	restrict: 'A',
	  	link: function(scope, el, attrs, controller){
          	function show(){
	  			var arr = el.parent()[0].getElementsByClassName('sorting');
	  			for (var i = 0; i < arr.length; i++) {	
	        		arr[i].classList.remove('sort_asc');
	        		arr[i].classList.remove('sort_desc');
	        	};
	  		}
	        scope.$watch(function() {return el.attr('class'); }, function(newValue){
	        	if(scope.filter){
	       			if(scope.filter.sort_on){       			
	       				show();
	       				switch(scope.filter.sort_on){
	       					case 'asc'  :  el.addClass('sort_asc'); break;
	       					case 'desc' :  el.addClass('sort_desc'); break;
	       				}
	       				
	       			}
	       		}
	        });
	        el.addClass('sorting');
	        el.bind("click", function(e){
	          	if (el.hasClass('sorting')) {
	            	if(el.hasClass('sort_asc')) {
		            	el.removeClass('sort_asc');
		            	el.addClass('sort_desc');  
		            } else {
		            	el.removeClass('sort_desc');
		            	el.addClass('sort_asc');
		            }
	            } 
	            scope.PreCheck(this.getAttribute('fieldName') ); 	            
	        });
        }
	  };
	});



	/*
	.directive('validDataReverse', function(config) {
		return {
			require: 'ngModel',
			link: function($scope, el, attrs, ctrl) {
				if (!ctrl) return;

				var is_valid = function(s) {
					return ( (s<$scope.date) || s==undefined) ? true : false;
				};

				ctrl.$parsers.push(function(viewValue) {
					ctrl.$setValidity('valid-data', is_valid(viewValue));
					return is_valid(viewValue) ? viewValue : undefined;
				});
				ctrl.$formatters.push(function(modelValue) {
					ctrl.$setValidity('valid-data', is_valid(modelValue));
					return modelValue;
				});

			}
		};
	})*/
	// 	.directive('ngIsObject', function(config) {
	// 	return {
	// 		require: 'ngModel',
	// 		link: function($scope, el, attrs, ctrl) {
	// 			if (!ctrl) return;

	// 			var is_valid = function(s) {
	// 				return angular.isObject(s);
	// 			};

	// 			ctrl.$parsers.push(function(viewValue) {
	// 				ctrl.$setValidity('ng-is-object', is_valid(viewValue));
	// 				return is_valid(viewValue) ? viewValue : undefined;
	// 			});
	// 			ctrl.$formatters.push(function(modelValue) {
	// 				ctrl.$setValidity('ng-is-object', is_valid(modelValue));
	// 				return modelValue;
	// 			});

	// 		}
	// 	};
	// })

;