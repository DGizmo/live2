angular.module('tk.utils', [])
	.factory('TkUtils', ['$location', '$route', function($location, $route) {
		return {
			vu: function(part) {
				var parts = $location.path().split('/');
				if (parts[0] == '') {
					parts.splice(0, 1);
				}

				return parts[part];
			},
			mainUrl: function() {
				var parts = $location.path().split('/');

				if (/p\d+/.test(parts[ parts.length - 1 ])) {
					parts.splice(parts.length - 1, 1);
				}

				return parts.join('/');
			},
			serialize: function(drop) {
				var p = [];
				var dropKeys = {};
				angular.forEach(drop, function(value){
					dropKeys[value] = 1;
				});

				angular.forEach($route.current.params, function(value, key){
					if (dropKeys[key]) {
						return;
					}
					p.push(key + '=' + value);
				});

				return p.join('&');
			},
			serializeHash: function(hash) {
				var p = [];

				angular.forEach(hash, function(value, key){
					p.push(key + '=' + value);
				});

				return p.join('&');
			}
		};
	}]);

angular.module('tk.utils.base', ['tk.utils', 'tk.utils.tpls'])
	.directive('tkPaging', ['$route', '$location', '$locale', 'TkUtils', function($route, $location, $locale, utils) {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'tmpl/etc/pagination.html',
			link: function($scope, el, attrs, ctrl) {
				$scope.$locale = $locale;
				$scope.URL = utils.mainUrl();
				$scope.params = utils.serialize(['page']);
				
				if ($scope.params) {
					$scope.params = '?' + $scope.params;
				}
			}
		};
	}])
	.directive('tkUserpaging', ['$route', '$location', '$locale', 'TkUtils', function($route, $location, $locale, utils ) {
		return {
			restrict: 'AE',
			replace: true,
			templateUrl: 'tmpl/etc/pagination2.html',
			link: function($scope, el, attrs, ctrl) {
				$scope.$locale = $locale;
				$scope.URL = utils.mainUrl();
				$scope.params = utils.serialize(['page']);
				
				if ($scope.params) {
					$scope.params = '?' + $scope.params;
				}

				var pag3;

				$scope.$watch(function(){ return attrs.maxpage}, function() {
			   		if( attrs.maxpage<2) { return false; } 
		   			var page = attrs.showpage;
		   			if(parseInt(page) > parseInt(attrs.maxpage) ) { page = attrs.maxpage; }
		   			pag3 = new Paginator(attrs.id , attrs.maxpage, page, attrs.curpage, location.origin+attrs.navig);
			   	});

			}
		};
	}])
;

angular.module('tk.utils.table', ['tk.utils', 'tk.utils.tpls'])
	.controller('tkTableCtrl', ['$scope','$route','$location', '$routeParams', 'TkUtils',
		function ($scope, $route, $location, $routeParams, utils) {
			if (!$scope.filter) {
				$scope.filter = angular.copy($route.current.params);
				if ($scope.filter['page']) {
					delete $scope.filter['page'];
				}
			}
			var URL = utils.mainUrl();
			if ($route.current.params['page']) {
				URL = URL + '/p1';
			}
			$scope.sortBy = function(name) {
				if ($scope.filter['sortby'] && $scope.filter['sortby'] != name) {
					$scope.filter['sort'] = 'desc';
				}

				$scope.filter['sortby'] = name;
				$scope.filter['sort'  ] = $scope.filter['sort'] === 'asc' ? 'desc' : 'asc';

				var p = utils.serializeHash($scope.filter);

				if (p) {
					p = '?' + p;
				}
				$location.url(URL + p);
			};

			$scope.reset_filter = function() {
				$scope.filter = {};
				$scope.search();
			};

			$scope.search = function() {
				var p = utils.serializeHash($scope.filter);
				if (p) {
					p = '?' + p;
				}
				$location.url(URL + p);
			};
		}
	])
	.directive('tkTable', ['$compile', function($compile) {
		return {
			restrict: 'A',
			controller: 'tkTableCtrl',
			link: function($scope, el, attrs, ctrl) {
				el.addClass('tk-table');

				angular.forEach(el.find('th'), function (e) {
					var th   = angular.element(e);
					var name = th.attr('name');
					if (name) {
						th.addClass('sortable');
						if ($scope.filter['sortby'] && $scope.filter['sortby'] === name) {
							th.addClass(
								$scope.filter['sort'] === 'desc' ? 'sort-desc' :
								$scope.filter['sort'] === 'asc'  ? 'sort-asc'  : ''
							);
						}

						var headerTemplate = angular.element('<div ng-click="sortBy(\'' + name + '\')"></div>').html(th.html());
						th.html(headerTemplate);
						$compile(headerTemplate)($scope);
					}
				});
			}
		};
	}])
;




angular.module("tk.utils.tpls", ["tmpl/etc/pagination.html"]);

angular.module('tmpl/etc/pagination.html', []).run(["$templateCache", function($templateCache) {
	$templateCache.put('tmpl/etc/pagination.html',
		'<div class="clearfix" ng-if="page.paging.length > 1">' +
		'<ul class="pagination pagination-sm">' +
		'<li class="{{ page.page == cur ? \'active\' : \'\' }}" ng-repeat="cur in page.paging"><a href="#{{ URL }}/p{{ cur }}{{ params }}">{{ cur }}</a></li>' +
		'</ul>' +
		'<h4 class="pull-right" style="margin-top: 24px;"><span class="label label-default">{{ $locale.tk_paging.total || \'Total\' }}: {{ page.max }}</span></h4>' +
		'</div>'
	);
		$templateCache.put('tmpl/etc/pagination2.html',
		'<div class="paginator" id="pgntr"></div>'
	);
}]);