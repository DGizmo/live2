APP.factory('DocumentList', ['$resource', function($resource){
	return $resource('/api/documents',
		{}, {
		item:   {method:'GET',  url: '/api/documents/:id', params: {id: '@id'}},
		add:    {method:'POST', url: '/api/documents/add'},
		save:   {method:'POST', url: '/api/documents/:id/edit',   params: {id: '@id'}},
		remove: {method:'POST', url: '/api/documents/:id/remove', params: {id: '@_id'}},
	});
}]);

APP.controller('DatabookList',['$scope','$http','$location','$route','DatabList',
	function ($scope,$http,$location,$route,DatabList) {
		$scope.list = DatabList.query();

		$scope.addType = function (type) {
			DatabList.add({
				name:type.newname,
				deleted: 0,
			}, function() {
				$scope.list = DatabList.query();
				$('#txttype').val("");
				$('#addtype').click();
			});
		}

		$scope.EditType = function (type) {
			if (type.name) {
				DatabList.save({
					id:type._id.$oid,
					name:type.name,
				});
			type.$edit = false;
			}
		}

		$scope.remove = function (itemid,type) {
			if (confirm("Вы точно хотите удалить этот элемент?")) {
				DatabList.remove({_id:itemid}, function() {
	          	$scope.list.splice($scope.list.indexOf(type), 1);
	        	});
			}
		}
	}]
);

APP.controller('Clients',['$scope','$rootScope','$http','$location','$route','ClientsList',
	function ($scope,$rootScope,$http,$location,$route,ClientsList) {

			ClientsList.get($route.current.params, function(data) {
				$scope.page = data.page;
				$scope.list = data.list;
			});

		$scope.$on('popupEvent', function(event,args){
            $scope.client={
            	name: args
            };
        });

		$scope.addClient = function (client) {

			// if (!(/^d+$/).test(client.INN) && (client.INN.length==10 || client.INN.length==12)) {
				 var existINN;
				for (var i=0; i<$scope.list.length; i++) {

					if ($scope.list[i].INN === client.INN) {existINN = $scope.list[i].name;}

				}
				if (existINN) {
					alert('такой ИНН уже существует у клиента '+ existINN);
				} else {

					ClientsList.add({
						name:client.name,
						INN:client.INN,
						comment:client.comment,
						OurClient: client.our,
						deleted: 0,
					}, function() {
						console.log("href = ",location);
						if( location.hash == '#/documents/add' ) { 
							$scope.hidemodel();
							client.name ='';
							client.INN ='';
							client.comment='';
							client.our =false;
						}
						else if(location.hash == '#/mail'){
							$scope.hidemodel();
							client.name ='';
							client.INN ='';
							client.comment='';
							client.our =false;	
						}
						else location.href = '/#/clients';
					});	
				}

			// } else {return alert('ИНН может быть только числовым и от 10 до 12 знаков');}
		}

		$scope.EditClient = function (client) {
			if (client.name || client.INN) {
					ClientsList.save({
					id:client._id.$oid,
					name:client.name,
					INN:client.INN,
					OurClient: client.OurClient,
					comment:client.comment,
				});

				client.$edit = false;
			}
		}

		$scope.Cremove = function (itemid,client) {
			if (confirm("Вы точно хотите удалить этот элемент?")) {
				ClientsList.remove({_id:itemid}, function() {
	          	$scope.list.splice($scope.list.indexOf(client), 1);
	        	});
			}
		}

		$scope.LoadList = function (val) {
			
			if (val != "") {
				return $http.get('/api/clients/?q=' + val , {}
				).then(function(res){
				      var addresses = [];
				      angular.forEach(res.data, function(item){
				        addresses.push(item);
				        $scope.list = item;
				      });
				      return [$scope.list = addresses, $scope.page = 0];
					});
			} else {
				ClientsList.get($route.current.params, function(data) {
					$scope.page = data.page;
					$scope.list = data.list;
				});
			}

		}
	}]
);

APP.controller('Company', ['$scope','$http','$location','$route', 'Companylist',
	function ($scope,$http,$location,$route,Companylist) {
		$scope.list = Companylist.query();

		$scope.addcompany = function (company) {
			Companylist.add({
				name:company.newname,
				deleted: 0,
			}, function() {
				$scope.list = Companylist.query();
				$('#txtCompany').val("");
				$('#addtype').click();
			});
		}

		$scope.Editcompany = function (company) {
			if (company.name) {
				Companylist.save({
					id:company._id.$oid,
					name:company.name,
				});
			company.$edit = false;
			}
		}

		$scope.remove = function (itemid,company) {
			if (confirm("Вы точно хотите удалить этот элемент?")) {
				Companylist.remove({_id:itemid}, function() {
	          	$scope.list.splice($scope.list.indexOf(company), 1);
	        	});
			}
		}

}]
);