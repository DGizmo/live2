'use strict';

APP.factory('ClientsList', ['$resource', function($resource) {
	return $resource('/api/clients', {}, {
		item: {
			method: 'GET',
			url: '/api/clients/:id',
			params: {
				id: '@_id.$oid'
			}
		},
		add: {
			method: 'POST',
			url: '/api/clients/add'
		},
		save: {
			method: 'POST',
			url: '/api/clients/:id/edit',
			params: {
				id: '@id'
			}
		},
		remove: {
			method: 'POST',
			url: '/api/clients/:id/remove',
			params: {
				id: '@_id'
			}
		},
	});
}]);

APP.factory('DatabList', ['$resource', function($resource) {
	return $resource('/api/types', {}, {
		item: {
			method: 'GET',
			url: '/:id',
			params: {
				id: '@_id.$oid'
			}
		},
		add: {
			method: 'POST',
			url: '/api/types/add'
		},
		save: {
			method: 'POST',
			url: '/api/types/:id/edit',
			params: {
				id: '@id'
			}
		},
		remove: {
			method: 'POST',
			url: '/api/types/:id/remove',
			params: {
				id: '@_id'
			}
		},
	});
}]);

APP.factory('Companylist', ['$resource', function($resource) {
	return $resource('/api/company', {}, {
		item: {
			method: 'GET',
			url: '/:id',
			params: {
				id: '@_id.$oid'
			}
		},
		add: {
			method: 'POST',
			url: '/api/company/add'
		},
		save: {
			method: 'POST',
			url: '/api/company/:id/edit',
			params: {
				id: '@id'
			}
		},
		remove: {
			method: 'POST',
			url: '/api/company/:id/remove',
			params: {
				id: '@_id'
			}
		},
	});
}]);

APP.factory('RecLogView', ['$resource', function($resource) {
	return $resource('/api/logs', {}, {
		item: {
			method: 'GET',
			url: '/api/logs/:id',
			params: {
				id: '@_id'
			}
		},
		add: {
			method: 'POST',
			url: '/api/logs/add'
		},
		save: {
			method: 'POST',
			url: '/api/logs/:id/edit',
			params: {
				id: '@id'
			}
		},
		remove: {
			method: 'POST',
			url: '/api/logs/:id/remove',
			params: {
				id: '@_id'
			}
		},
	});
}]);

APP.factory('DocumentList', ['$resource', function($resource) {
	return $resource('/api/documents', {}, {
		item: {
			method: 'GET',
			url: '/api/documents/:id',
			params: {
				id: '@id'
			}
		},
		add: {
			method: 'POST',
			url: '/api/documents/add'
		},
		packageadd: {
			method: 'POST',
			url: '/api/documents/package_add'
		},
		mailadd: {
			method: 'POST',
			url: '/api/documents/mail_add'
		},
		save: {
			method: 'POST',
			url: '/api/documents/:id/edit',
			params: {
				id: '@id'
			}
		},
		remove: {
			method: 'POST',
			url: '/api/documents/:id/remove',
			params: {
				id: '@_id'
			}
		},
		getmail: {
			method: 'GET',
			url: '/api/documents/mail_list',
		},
	});
}]);

APP.controller('getUser', ['$scope', '$http', '$location', '$route', '$routeParams',
	function($scope, $http, $location, $route, $routeParams) {
		return $http.get('/api/user/getinfo').then(function(res) {
			$scope.user = res.data.role;
			$scope.user_id = res.data.user;
		});
	}
]);

APP.controller('DocList', ['$scope', '$rootScope', '$http', '$location', '$route', 'FileUploader', '$routeParams', 'DocumentList', 'ClientsList', 'DatabList',
	function($scope, $rootScope, $http, $location, $route, FileUploader, $routeParams, DocumentList, ClientsList, DatabList) {
		function load(filter) {
			DocumentList.get({
				enabled: 1,
				company: filter.clients_s && filter.clients_s._id ? filter.clients_s._id.$oid : '',
				INN: filter.INN ? filter.INN : '',
				cinema: filter.cinema_s ? filter.cinema_s.id : '',
				date_reciept: filter.date ? filter.date / 1000 : '',
				type_of_doc: filter.type_of_doc ? filter.type_of_doc : '',
				number: filter.number ? filter.number : '',
				sort_by: filter.sort_by , 
				sort_on: filter.sort_on 
			}, function(data) {
				if (!$route.current.params.page) {
					data.page.page = 1;
				} else {
					data.page.page = $route.current.params.page;
				}
				$scope.page = data.page;
				$scope.list = data.list;
			});
		}

		DocumentList.get($route.current.params, function(data) {
			if ($rootScope.fil) {
				$scope.filter = $rootScope.fil;
				load($scope.filter);
			} else {
				$scope.page = data.page;
				$scope.list = data.list;
			}
		});

		$scope.types = DatabList.query();

		var uploader = $scope.uploader = new FileUploader({
			scope: $scope,
			url: '/api/documents/:act/file',

		});
		uploader.autoUpload = true;
		uploader.onCompleteItem = function(item, response, status, headers) {
			$scope.file = response.files;
			if (response.status == "ok") {
				DocumentList.get($route.current.params, function(data) {
					$scope.page = data.page;
					$scope.list = data.list;
				});
			}
		};
		if (uploader.queue.length) {
			uploader.uploadAll(function(data) {});
		}

		$scope.ClearFilter = function(filter) {
			$scope.filter = "";
			DocumentList.get($route.current.params, function(data) {
				$scope.page = data.page;
				$scope.list = data.list;
			});
		}

		$scope.remove = function(itemid, document) {
			if (confirm("Вы точно хотите удалить этот элемент?")) {
				DocumentList.remove({
					_id: itemid
				}, function() {
					$scope.list.splice($scope.list.indexOf(document), 1);
				});
			}
		}

		$scope.getClients = function(val) {
			return $http.get('/api/clients/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}
		$scope.getCinema = function(val) {
			return $http.get('/api/cinema/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}

		var fil, sort = 'asc';
		$scope.PreCheck = function(val) {
			if (fil == val) {
				if (sort == 'desc') sort = 'asc';
				else sort = 'desc';
			} else {
				fil = val;
				sort = 'asc';
			}

			if($scope.filter){
				$scope.filter.sort_by = fil;
				$scope.filter.sort_on = sort;
				$scope.Search($scope.filter);
			}
			else{
				$scope.Search({
					'sort_by': fil,
					'sort_on': sort
				});	
			}
		}

		$scope.Search = function(filter) {
			if (!filter) {
				return false;
			}
			location.href = '/#/documents';
			load(filter);
			$scope.filter = filter;
			$rootScope.fil = filter;
		}
	}
]);

APP.controller('MailList', ['$scope', '$rootScope', '$window', '$http', '$location', '$route', '$routeParams', 'DocumentList', 'ClientsList', 'DatabList', 'Companylist',
	function($scope, $rootScope, $window, $http, $location, $route, $routeParams, DocumentList, ClientsList, DatabList, Companylist) {

		var forms = [];
		var prev_id = 0;

		DocumentList.getmail($route.current.params, function(data) {
			$scope.page = data.page;
			$scope.list = data.list;
		});

		var now = new Date();
		var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		$scope.date ? $scope.date.valueOf() : ($scope.date = startOfDay);

		$scope.types = DatabList.query();
		$scope.ourcompanes = Companylist.query();

		$scope.Popup = function() {
			$rootScope.$broadcast('popupEvent', $scope.form.myVal);
		}

		$scope.getClients = function(val) {
			return $http.get('/api/clients/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}

		$scope.getCinema = function(val) {
			return $http.get('/api/cinema/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}

		$scope.open_doc = function(id, filename) {
			var link = 'docs_file/tmp/' + id + '/' + filename;
			$window.open(link);
		}

		$scope.doc_link = function(id, filename, date) {

			$scope.old_id = id;
			$scope.filename = '"' + filename + '"';

			// remember form //
			forms[prev_id] = {
				date: $scope.date,
				company: $scope.company,
				cinema: $scope.cinema,
				ourcompany: $scope.ourcompany,
				type_of_doc: $scope.type_of_doc,
				valid_date: $scope.valid_date,
				number: $scope.number,
				original_doc: $scope.original_doc,
				comment: $scope.comment
			}
			prev_id = id;
			// remember form end //

			var f = {
				'company': 1,
				'cinema': 1,
				'ourcompany': 1,
				'type_of_doc': 1,
				'valid_date': 1,
				'number': 1,
				'original_doc': 1,
				'comment': 1
			};

			// refresh form //
			if (forms[id]) {
				$scope.date = forms[id].date;
				for (var k in f) {
					$scope[k] = forms[id][k];
				}
			} else {
				$scope.date = date;
				for (var k in f) {
					$scope[k] = '';
				}
			}
			// refresh form end //
		}

		$scope.Submit = function() {
			$scope.Add();

		}

		$scope.Add = function() {
			var date = $scope.date ? ($scope.date.valueOf()) / 1000 : '';
			DocumentList.get({
					number: $scope.number,
					type_of_doc: $scope.type_of_doc,
					date_reciept: $scope.date,
					company: $scope.company._id.$oid,
					deleted: 0
				},
				function(existDoc) {
					if (existDoc.list[0]) {
						alert('Такой документ в системе уже существует');
					} else {
						if (!$scope.old_id) {
							alert('Документ не выбран');
						} else {

							if ($scope.date_reciept) {
								$scope.date_reciept = ($scope.date_reciept.valueOf()) / 1000
							}
							if ($scope.valid_date) {
								$scope.valid_date = ($scope.valid_date.valueOf()) / 1000
							}

							DocumentList.mailadd({
									company: {
										_id: $scope.company._id.$oid,
										name: $scope.company.name,
										INN: $scope.company.INN,
									},
									cinema: $scope.cinema,
									ourcompany: $scope.ourcompany,
									type_of_doc: $scope.type_of_doc,
									valid_date: $scope.valid_date,
									number: $scope.number,
									original_doc: $scope.original_doc,
									date_reciept: date,
									comment: $scope.comment,
									deleted: 0,
									old_id: $scope.old_id,
								},
								function(msg) {
									if (msg.status == "ok") {
										$route.reload();
									}
								});
							showSuccess('Документ успешно добавлен');
							$route.reload();
						}
					}
				});
		}

	}
]);

APP.controller('AddExcel', ['$scope', '$http', '$location', '$route', 'DocumentList', 'DatabList', 'Companylist', 'FileUploader',
	function($scope, $http, $location, $route, DocumentList, DatabList, Companylist, FileUploader) {

		$scope.types = DatabList.query();
		$scope.ourcompanes = Companylist.query();
		$scope.preloader = false;

		var files = [];
		var now = new Date();
		var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		$scope.date ? $scope.date.valueOf() : ($scope.date = startOfDay);

		var uploader_xls = $scope.uploader_xls = new FileUploader({
			scope: $scope,
			url: '/api/documents/package_xls'
		});


		var uploader_files = $scope.uploader_files = new FileUploader({
			scope: $scope,
			url: '/api/documents/get_file',
		});

		uploader_xls.onAfterAddingFile = function(item) {

			if (uploader_xls.queue[1]) {
				if (uploader_xls.queue[0].file.name == item.file.name) { //same file
					if (uploader_xls.queue[0].file.size == item.file.size) {
						item.remove();
						return false;
					}
					uploader_xls.uploadItem(item); //same name. Hitrojopy izmenil file
					uploader_xls.queue[0].remove();
				} else {
					uploader_xls.uploadItem(item); //inverse
					uploader_xls.queue[0].remove();
				}
			} else {
				uploader_xls.uploadItem(item);

			}
			$scope.errFiles = '';
		}

		uploader_xls.onCompleteItem = function(item, response, status, headers) {

			if (response.status == 'error') {
				var msgs = {
					'empty_excel': "Пустой или невалидный excel-файл",
					'double': "Обнаружены дублирующие документы в excel файле",
					'duplications': "Документы в указанном excel файле существуют в базе данных",
					'error_date': "Неверный формат даты в excel-файле",
					'parse_error': "Excel-файл поврежден",
					'no_excel': "Не загружено ни одного excel-файла",
				};

				if (response.error.empty_excel == 'empty_excel') {
					$scope.errXSL = msgs['empty_excel'];
				} else {
					for (var key in response.error) {
						$scope.errXSL = msgs[key] + " " + response.error[key].toString().split(',').join(", ");
					}
				}

				if (uploader_files.queue) {
					for (var i = uploader_files.queue.length - 1; i >= 0; i--) {
						uploader_files.queue[i].remove();
					}
				}

				item.remove();
				return false;
			} else {
				$scope.errXSL = '';
				uploader_xls.show = true;
				$scope.oid = response.id.$oid;
			}

			var mas = [];
			for (var key in response.xls) {
				mas.push(response.xls[key].title);
			}
			$scope.inside = mas;

			process();
		}


		function process() {
			if (uploader_files.queue.length > 0) {
				$scope.DeleteXLS();

				var len = uploader_files.queue.length;
				var flag = 0;
				for (var i = 0; i < $scope.inside.length; i++) {
					flag = 0;

					for (var j = 0; j < len; j++) {
						if (uploader_files.queue[j].file.name == $scope.inside[i]) {
							uploader_files.queue[j].file.selected = 1;
							flag++;
						}
					}

					if (flag == 0) {
						uploader_files.addToQueue($scope.inside[i]);
						uploader_files.queue[uploader_files.queue.length - 1].file.selected = 0;
					}
				}
			} else {
				for (var key in $scope.inside) {
					uploader_files.addToQueue($scope.inside[key]);
					uploader_files.queue[key].file.selected = 0;
				}
			}
		}

		uploader_files.onAfterAddingFile = function(item) {
			if (!$scope.inside) {
				item.remove();
				return false;
			}
			if (!item.file.size) {
				return false;
			}

			$scope.errFiles = '';
			for (var key = 0; key < uploader_files.queue.length - 1; key++) {
				if (item.file.name == uploader_files.queue[key].file.name) {
					if (uploader_files.queue[key].file.selected != 0) { //duplicate
						item.remove();
						return false;
					}

					item.file.selected = 1;
					uploader_files.queue[key].remove();

					return false;
				}
			}

			item.file.selected = 2; //new --> red	

		}

		$scope.DeleteGreen = function(item) {
			item.file.selected = 0;
			item.file.lastModifiedDate = null;
			item.file.size = null;
			$scope.errFiles = '';
		}

		$scope.DeleteXLS = function() {

			var mas = []; //хранит серые файлы
			for (var key in uploader_files.queue) {
				if (uploader_files.queue[key].file.selected == 1) { //green --> red
					uploader_files.queue[key].file.selected = 2;
				} else if (uploader_files.queue[key].file.selected == 0) {
					mas.push(key);
				}
			}

			for (var i = mas.length - 1; i >= 0; i--) {
				uploader_files.queue[mas[i]].remove();
			}
			$scope.errFiles = '';
		}


		$scope.Submit = function() {
			if (!uploader_xls.queue.length) {
				$scope.errXSL = "Excel-файл не выбран";
				return false;
			}

			for (var key in uploader_files.queue) {
				if (uploader_files.queue[key].file.selected == 0) {
					$scope.errFiles = "Заполните до конца файлы";
					return false;
				}
				if (uploader_files.queue[key].file.selected == 2) {
					$scope.errFiles = "Удалите ненужные файлы";
					return false;
				}
			}

			if ($scope.validate()) $scope.Add();
		}

		$scope.Add = function() {
			$scope.preloader = true;
			var files = [];
			for (var key in uploader_files.queue) {
				files.push(uploader_files.queue[key].file);
			}
			uploader_files.uploadAll();

			uploader_files.onCompleteAll = function() {

				DocumentList.packageadd({
						company: {
							_id: $scope.company._id.$oid,
							name: $scope.company.name,
							INN: $scope.company.INN,
						},
						ourcompany: $scope.ourcompany,
						type_of_doc: $scope.type_of_doc,
						files: files,
						deleted: 0,
						oid: $scope.oid,
					},
					function(data) {
						if (data.status == "ok") {
							location.href = '/#/documents/';
						}
					}
				);
			}

		}

		$scope.getClients = function(val) {
			return $http.get('/api/clients/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}
		$scope.getCinema = function(val) {
			return $http.get('/api/cinema/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}
	}
]);

APP.controller('AddDoc', ['$scope', '$rootScope', '$http', '$location', '$route', 'DocumentList', 'DatabList', 'Companylist', 'FileUploader',
	function($scope, $rootScope, $http, $location, $route, DocumentList, DatabList, Companylist, FileUploader) {
		$scope.types = DatabList.query();
		$scope.ourcompanes = Companylist.query();

		var now = new Date();
		var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		$scope.date ? $scope.date.valueOf() : ($scope.date = startOfDay);

		var uploader = $scope.uploader = new FileUploader({
			scope: $scope,
			url: '/api/documents/:act/upload',

		});

		uploader.onCompleteItem = function(item, response, status, headers) {
			$scope.file = response.files;
			$scope.Add();
		};

		$scope.Popup = function() {
			$rootScope.$broadcast('popupEvent', $scope.form.myVal);
		}

		$scope.Submit = function() {
			if (uploader.queue.length) {
				uploader.uploadAll();
			} else {
				$scope.Add();
			}
		}

		$scope.Add = function() {
			var date = $scope.date ? ($scope.date.valueOf()) / 1000 : '';

			DocumentList.get({
					number: $scope.number,
					type_of_doc: $scope.type_of_doc,
					date_reciept: date,
					company: $scope.company._id.$oid,
					deleted: 0
				},
				function(existDoc) {
					if (existDoc.list[0]) {
						alert('Такой документ в системе уже существует');
					} else {
						if ($scope.date_reciept) {
							$scope.date_reciept = ($scope.date_reciept.valueOf()) / 1000
						}
						if ($scope.valid_date) {
							$scope.valid_date = ($scope.valid_date.valueOf()) / 1000;
						}

						var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
						var listOld =
							DocumentList.add({
								company: {
									_id: $scope.company._id.$oid,
									name: $scope.company.name,
									INN: $scope.company.INN,
								},
								cinema: $scope.cinema,
								ourcompany: $scope.ourcompany,
								type_of_doc: $scope.type_of_doc,
								valid_date: $scope.valid_date,
								number: $scope.number,
								//date         : now,
								original_doc: $scope.original_doc,
								file: $scope.file,
								date_reciept: date,
								comment: $scope.comment,
								deleted: 0,
								action: 'add',
							});
						location.href = '/#/documents';
						return false;

					}
				});
		}

		$scope.getClients = function(val) {
			return $http.get('/api/clients/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}
		$scope.getCinema = function(val) {
			return $http.get('/api/cinema/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}
	}
]);

APP.controller('EditDoc', ['$scope', '$http', '$location', '$route', '$routeParams', 'DocumentList', 'ClientsList', 'DatabList', 'Companylist', 'FileUploader',
	function($scope, $http, $location, $route, $routeParams, DocumentList, ClientsList, DatabList, Companylist, FileUploader) {

		$scope.$showAll = false;
		$scope.types = DatabList.query();
		$scope.ourcompanes = Companylist.query();
		var iditem = $routeParams.id;
		var defaultList = 2;
		$scope.$defaultList = defaultList;
		$scope.$limit = defaultList;
		DocumentList.item({
			id: iditem
		}, function(res) {
			$scope.item = res;
			$scope.item.date = $scope.item.date * 1000;
			$scope.item.date_reciept = $scope.item.date_reciept * 1000;
			$scope.item.valid_date = $scope.item.valid_date * 1000;
		});


		var uploader = $scope.uploader = new FileUploader({
			scope: $scope,
			url: '/api/documents/:act/upload',

		});

		uploader.onCompleteItem = function(item, response, status, headers) {
			$scope.file = response.files;
			$scope.EditDoc();
		};

		$scope.Submit = function(document) {

			if (uploader.queue.length) {
				uploader.uploadAll();
			} else {
				$scope.EditDoc();
			}
		}

		$scope.EditDoc = function(document) {
			DocumentList.save({
				id: $scope.item._id.$oid,
				company: {
					_id: $scope.item.company._id.$oid,
					name: $scope.item.company.name,
				},
				cinema: $scope.item.cinema,
				ourcompany: $scope.item.ourcompany,
				type_of_doc: $scope.item.type_of_doc,
				valid_date: $scope.item.valid_date / 1000,
				number: $scope.item.number,
				date: $scope.item.date / 1000,
				date_reciept: $scope.item.date_reciept / 1000,
				original_doc: $scope.item.original_doc,
				file: $scope.file,
				comment: $scope.item.comment,
				action: 'edit',
			});
			$scope.item.$edit = false;
			location.href = '/#/documents';
		}

		$scope.getClients = function(val) {
			return $http.get('/api/clients/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}
		$scope.getCinema = function(val) {
			return $http.get('/api/cinema/?q=' + val, {}).then(function(res) {
				var addresses = [];
				angular.forEach(res.data, function(item) {
					addresses.push(item);
				});
				return addresses;
			});
		}

	}
]);

APP.controller('ViewDoc', ['$scope', '$http', '$location', '$route', '$routeParams', 'DocumentList', 'ClientsList', 'DatabList', 'Companylist', 'RecLogView', 'FileUploader',
	function($scope, $http, $location, $route, $routeParams, DocumentList, ClientsList, DatabList, Companylist, RecLogView, FileUploader) {

		$scope.$showAll = false;
		$scope.types = DatabList.query();
		$scope.ourcompanes = Companylist.query();
		var iditem = $routeParams.id;
		var defaultList = 10;
		$scope.$defaultList = defaultList;
		$scope.$limit = defaultList;
		DocumentList.item({
			id: iditem
		}, function(res) {
			$scope.item = res;
			$scope.item.date = $scope.item.date * 1000;
			$scope.item.date_reciept = $scope.item.date_reciept * 1000;
			$scope.item.valid_date = $scope.item.valid_date * 1000;
		});
		RecLogView.item({
			id: iditem
		});
	}
]);