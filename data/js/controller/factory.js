APP.factory('ClientsList', ['$resource', function($resource){
	return $resource('/api/clients',
		{}, {
		item:   {method:'GET',  url: '/api/clients/:id', params: {id: '@_id.$oid'}},
		add:    {method:'POST', url: '/api/clients/add'},
		save:   {method:'POST', url: '/api/clients/:id/edit',   params: {id: '@id'}},
		remove: {method:'POST', url: '/api/clients/:id/remove', params: {id: '@_id'}},
	});
}]);

APP.factory('DatabList', ['$resource', function($resource){
	return $resource('/api/types',
		{}, {
		item:   {method:'GET',  url: '/:id', params: {id: '@_id.$oid'}},
		add:    {method:'POST', url: '/api/types/add'},
		save:   {method:'POST', url: '/api/types/:id/edit',   params: {id: '@id'}},
		remove: {method:'POST', url: '/api/types/:id/remove', params: {id: '@_id'}},
	});
}]);

APP.factory('Companylist', ['$resource', function($resource){
	return $resource('/api/company',
		{}, {
		item:   {method:'GET',  url: '/:id', params: {id: '@_id.$oid'}},
		add:    {method:'POST', url: '/api/company/add'},
		save:   {method:'POST', url: '/api/company/:id/edit',   params: {id: '@id'}},
		remove: {method:'POST', url: '/api/company/:id/remove', params: {id: '@_id'}},
	});
}]);

APP.factory('DocumentList', ['$resource', function($resource){
	return $resource('/api/documents',
		{}, {
		item:       { method:'GET',  url: '/api/documents/:id',        params: {id: '@id'}  },
		add:        { method:'POST', url: '/api/documents/add'                              },
		packageadd: { method:'POST', url: '/api/documents/package_add'                      },
		save:       { method:'POST', url: '/api/documents/:id/edit',   params: {id: '@id'}  },
		remove:     { method:'POST', url: '/api/documents/:id/remove', params: {id: '@_id'} },
		getmail:    { method:'GET',  url: '/api/documents/mail_list',                       },
	});
}]);
