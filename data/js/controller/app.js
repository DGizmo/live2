'use strict';

var APP = angular.module('app', ['ngRoute','ngResource','ui.bootstrap','MyValidate.control', 'angularFileUpload','tk.utils.base','tk.utils.table']);
APP.config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/documents',           {templateUrl: '../tmpl/documents/list.html',          controller: 'DocList'})
            .when('/documents/p:page',    {templateUrl: '../tmpl/documents/list.html',          controller: 'DocList'})
            .when('/documents/add',       {templateUrl: '../tmpl/documents/add_document.html',  controller: 'AddDoc'})
            .when('/documents/add_excel', {templateUrl: '../tmpl/documents/add_excel.html',     controller: 'AddExcel'})
            .when('/documents/edit/:id',  {templateUrl: '../tmpl/documents/edit_document.html', controller: 'EditDoc'})
            .when('/documents/view/:id',  {templateUrl: '../tmpl/documents/view_document.html', controller: 'ViewDoc'})
            .when('/types',               {templateUrl: '../tmpl/databooks/types_list.html',    controller: 'DatabookList'})
            .when('/types/add',           {templateUrl: '../tmpl/databooks/types_list.html',    controller: 'DatabookList'})
            .when('/company',             {templateUrl: '../tmpl/databooks/company_list.html',  controller: 'Company'})
            .when('/clients',             {templateUrl: '../tmpl/databooks/clients_list.html',  controller: 'Clients'})
            .when('/clients/p:page',      {templateUrl: '../tmpl/databooks/clients_list.html',  controller: 'Clients'})
            .when('/clients/add',         {templateUrl: '../tmpl/databooks/add_client.html',    controller: 'Clients'})
            .when('/mail',                {templateUrl: '../tmpl/documents/mail_list.html',     controller: 'MailList'})
            .when('/mail/p:page',         {templateUrl: '../tmpl/documents/mail_list.html',     controller: 'MailList'})
            .otherwise({redirectTo: '/documents'});
    }
]);

APP.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|pdf|xls|csv|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);

APP.directive('customPopover', function () {
    return {
        restrict: 'A',
        //template: '<span>{{label}}</span>',
        link: function (scope, el, attrs) {
            scope.label = attrs.popoverLabel;
            $(el).popover({
                trigger: 'hover',
                html: true,
                content: attrs.popoverHtml,
                placement: attrs.popoverPlacement,
                title: attrs.popoverTitle
            });
        }
    };
});
APP.directive('addPopup', function($compile) {
        return {
            restrict: 'A',
            link:function(scope, el, attrs) {
                scope.$watch(attrs.ngModel, function(value){
                    scope.form.myVal = value;      
                });
            }
        }
});


angular.module('MyValidate.config', []).constant('config', {
    class: 'has-error',
    ERRORS: {
        'ng-is-object': 'Запись не существует',
        required:       'Поле обязательное для заполнения',
        'valid-data':   'Срок документа не может быть меньше Даты документа',
        existname:      'Введите существующее имя',
    },
});



