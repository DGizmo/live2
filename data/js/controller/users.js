
APP.controller ('getUser', ['$scope','$http','$location','$route','$routeParams',
	function ($scope,$http,$location,$route,$routeParams) {
		return $http.get('/api/user/getinfo'
		).then(function(res){
				$scope.user = res.data.role;
				$scope.user_id = res.data.user;
			});
	}
]);